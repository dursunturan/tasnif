<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model {

    protected $table = 'courses';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'quota',
        'instructor_id',
        'department_id',
    ];

    public function instructor()
    {
        return $this->belongsTo('App\Instructor');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

}

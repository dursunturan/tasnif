<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Department extends Model {

    protected $table = 'departments';

    public $timestamps = false;

    protected $fillable = [
        'title',
    ];

    public function head()
    {
        return $this->hasOne('App\Head');
    }

    public function instructors()
    {
        return $this->hasMany('App\Instructor');
    }

    public function courses()
    {
        return $this->hasMany('App\Course');
    }

}

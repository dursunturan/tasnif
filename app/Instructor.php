<?php namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Instructor extends Model implements AuthenticatableContract, CanResetPasswordContract {

    use Authenticatable, CanResetPassword;

    protected $table = 'instructors';

    protected $fillable = [
        'full_name',
        'email',
        'degree_id',
        'extension_number',
        'personal_phone_number',
        'department_id',
        'password',
    ];

    protected $hidden = ['password', 'remember_token'];

    public function courses()
    {
        return $this->hasMany('App\Course');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

    public function exams()
    {
        return $this->belongsToMany('App\Exam');
    }

    public static function generate_password()
    {
        $random_string = str_random(10);
        $password = \Hash::make($random_string);

        return $password;
    }

}

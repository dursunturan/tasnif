<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Classroom extends Model {

    protected $table = 'classrooms';

    public $timestamps = false;

    protected $fillable = [
        'code',
        'capacity',
    ];

    public function exams()
    {
        return $this->belongsToMany('App\Exam');
    }

}

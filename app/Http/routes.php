<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'HomeController@index');
Route::get('home', 'HomeController@index');

Route::group(['middleware' => 'App\Http\Middleware\AuthenticateAdmin'], function()
{
    Route::get('admin', 'AdminController@index');
    Route::get('admin/setup/{step_count?}', 'AdminController@setup');
    Route::post('admin', 'AdminController@store');
    Route::post('admin/setup/ajax', function() {
        if (Request::ajax()) {
            error_log(print_r(Input::all(), 1));
            Session::put(Input::all());
            return 'true';
        }
    });
});

Route::group(['middleware' => 'App\Http\Middleware\AuthenticateInstructor'], function(){
    Route::get('instructor', 'InstructorController@index');
    Route::get('instructor/edit-personal-info', 'InstructorController@edit');
    Route::put('instructor', 'InstructorController@update');
    Route::post('instructor', 'InstructorController@store');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);

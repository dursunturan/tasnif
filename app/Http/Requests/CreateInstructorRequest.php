<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateInstructorRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'full_name' => 'required|between:5,50|alpha_spaces',
            'email' => 'required|email|unique:instructors',
            'extension_number' => 'extension_number',
            'personal_phone_number' => 'phone:TR',
		];
	}

}

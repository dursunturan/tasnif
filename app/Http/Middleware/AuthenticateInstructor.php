<?php namespace App\Http\Middleware;

use Closure;

class AuthenticateInstructor {

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user() == null || $request->user()->email == 'admin@domain.com')
        {
            return redirect('home');
        }

        return $next($request);
    }

}

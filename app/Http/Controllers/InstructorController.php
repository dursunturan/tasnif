<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class InstructorController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
    {
        $courses = \Auth::user()->courses;
        $department = \Auth::user()->department;
        $exams = [];
        if (isset(\Auth::user()->exams)) {
            $exams = \Auth::user()->exams;
        } else {
            error_log('yok');
        }
        error_log($courses);
        error_log($department);
        error_log(print_r($exams, 1));

        return view('instructor.index', compact('courses', 'department', 'exams'));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @return Response
	 */
	public function edit()
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


    /**
     * Store the date of the selected exam.
     *
     * @param CreateInstructorRequest $request
     * @return Response
     */
    public function store(CreateInstructorRequest $request)
    {
        Instructor::create($request->all());

        return redirect('instructor');
    }

}

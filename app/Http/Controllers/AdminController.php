<?php namespace App\Http\Controllers;

use App\ExamDate;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Session;

class AdminController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $nearest_exam = ExamDate::nearest()->get()->first();

        return view('admin.index', compact('nearest_exam'));
	}

    /**
     * Display the term setup wizard.
     * @return Response
     */
    public function setup($step_count = 'setup')
    {
        switch($step_count) {
            case 'setup':
                return view('admin.setup');
            case 'step-1-exam-plan':
                return view('admin.setup.step-1-exam-plan');
            case 'step-2-exam-dates':
                return view('admin.setup.step-2-exam-dates');
            case 'step-3-departments':
                return view('admin.setup.step-3-departments');
            case 'step-4-classrooms':
                return view('admin.setup.step-4-classrooms');
            case 'step-5-instructors':
                return view('admin.setup.step-5-instructors');
            case 'step-6-courses':
                return view('admin.setup.step-6-courses');
            case 'step-7-heads':
                return view('admin.setup.step-7-heads');
            case 'step-8-confirm':
                return view('admin.setup.step-8-confirm');
            default:
                return redirect('admin/setup');
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        /*
         * Session variables
         *
         * date: //begin, //end, vize, final, but
           array:
           exams,
           departments,
           classrooms_code, classrooms_capacity,
           instructors_full_name, instructors_email, instructors_degree, instructors_extension_number, instructors_personal_phone_number, instructors_department_id,
           courses_title, courses_quota, courses_department_id, courses_instructor_id,
           heads_department_id, heads_instructor_id.
         */
        // Start and End date of a term
        // $begin = Session::get('begin');
        // $end = Session::get('end');
        // ExamDates: vize_begin, vize_end, final_begin, final_end, but_begin, but_end
        $exams = Session::get('exams');
        $vize_begin = Session::get('vize_begin');
        $vize_end = Session::get('vize_end');
        $final_begin = Session::get('final_begin');
        $final_end = Session::get('final_end');
        $but_begin = Session::get('but_begin');
        $but_end = Session::get('but_end');
        // Departments: bilgisayar mühendisliği, çevre mühendisliği ...
        $departments = Session::get('departments');
        /* Classrooms
         * Code: A101, B201
         * Capacity: 40, 100 ...
        */
        $classrooms_code = Session::get('classrooms_code');
        $classrooms_capacity = Session::get('classrooms_capacity');
        /* Instructors
         * Full name: Dursun Can Turan ...
         * Email: dursun.turan@bil.omu.edu.tr ...
         * Degree_id
         * Extension number: 2222, 3120 ...
         * Personal phone number: 05555555555
         * Department_id
         */
        $instructors_full_name = Session::get('instructors_full_name');
        $instructors_email = Session::get('instructors_email');
        $instructors_degree_id = Session::get('instructors_degree_id');
        $instructors_extension_number = Session::get('instructors_extension_number');
        $instructors_personal_phone_number = Session::get('instructors_personal_phone_number');
        $instructors_department_id = Session::get('instructors_department_id');
        /*
         * Courses
         * Title: Programlamaya Giriş, Matematik 1 ...
         * Quota: 20, 40 ...
         * Department_id
         * Instructor_id
         */
        $courses_title = Session::get('courses_title');
        $courses_quota = Session::get('courses_quota');
        $courses_department_id = Session::get('courses_department_id');
        $courses_instructor_id = Session::get('courses_instructor_id');
        /*
         * Heads
         * Department_id
         * Instructor_id
         */
        $heads_department_id = Session::get('heads_department_id');
        $heads_instructor_id = Session::get('heads_instructor_id');

        foreach ($exams as $key=>$exam) {
            $examdate_model = new \App\ExamDate;
            switch ($key) {
                case 0:
                    $examdate_model->title = 'Vize';
                    $examdate_model->begin = $vize_begin;
                    $examdate_model->end = $vize_end;
                    $examdate_model->save();
                    break;
                case 1:
                    $examdate_model->title = 'Final';
                    $examdate_model->begin = $final_begin;
                    $examdate_model->end = $final_end;
                    $examdate_model->save();
                    break;
                case 2:
                    $examdate_model->title = 'Büt';
                    $examdate_model->begin = $but_begin;
                    $examdate_model->end = $but_end;
                    $examdate_model->save();
                    break;
                default:
                    error_log('Wrong ExamDate');
                    break;
            }
        }

        foreach ($departments as $department) {
            $department_model = new \App\Department;
            $department_model->title = $department;
            $department_model->save();
        }

        foreach ($classrooms_code as $key=>$code) {
            $classroom_model = new \App\Classroom;
            $classroom_model->code= $code;
            $classroom_model->capacity= $classrooms_capacity[$key];
            $classroom_model->save();
        }

        foreach ($instructors_full_name as $key=>$full_name) {
            $instructor_model = new \App\Instructor;
            $instructor_model->full_name = $full_name;
            $instructor_model->email = $instructors_email[$key];
            $random_string = str_random(10);
            $password = \Illuminate\Support\Facades\Hash::make($random_string);
            $instructor_model->password = $password;
            $instructor_model->degree_id = $instructors_degree_id[$key];
            $instructor_model->extension_number = $instructors_extension_number[$key];
            $instructor_model->personal_phone_number = $instructors_personal_phone_number[$key];
            $instructor_model->department_id = $instructors_department_id[$key];
            $instructor_model->save();
            error_log($full_name. ': ' . $random_string);
        }

        foreach ($courses_title as $key=>$title) {
            $course_model = new \App\Course;
            $course_model->title = $title;
            $course_model->quota = $courses_quota[$key];
            $course_model->department_id = $courses_department_id[$key];
            $course_model->instructor_id = $courses_instructor_id[$key];
            $course_model->save();
        }

        foreach ($heads_department_id as $key=>$department_id) {
            $head_model = new \App\Head;
            $head_model->department_id = $department_id;
            $head_model->instructor_id = $heads_instructor_id[$key];
            $head_model->save();
        }

        return redirect('admin');
    }

}

<?php namespace App\Http\Controllers;

class HomeController extends Controller {

	/**
	 * Show the application dashboard to the user.
	 *
	 * @return Response
	 */
    public function get_keys_for_duplicate_values($my_arr, $clean = false) {
        if ($clean) {
            return array_unique($my_arr);
        }

        $dups = $new_arr = array();
        foreach ($my_arr as $key => $val) {
            if (!isset($new_arr[$val])) {
                $new_arr[$val] = $key;
            } else {
                if (isset($dups[$val])) {
                    $dups[$val][] = $key;
                } else {
                    $dups[$val] = array($key);
                }
            }
        }
        return $dups;
    }

	public function index()
	{
        // Sınav dönemi, Tarihler, Derslerin Adları, Sınıflar
        $nearest_exam = \App\ExamDate::nearest()->get()->first();
        $title = $nearest_exam->title;

        $exams = \App\Exam::vize()->get();
        //error_log($exams);
        $test = [];
        for ($i=0; $i<count($exams); $i++) {
            //error_log($exams[$i]->course_id);
            $test[$i] = $exams[$i]->course_id;
        }
        $test_values = [];
        $test_values = $this->get_keys_for_duplicate_values($test);
        $real_values = [];
        $i = 0;
        foreach ($test_values as $key=>$value) {
            $real_values[$i] = $value[0];
            $i++;
            // error_log($key . ' ' . $value[0]);
        }

        $codes = [];
        for ($i=0; $i<count($exams); $i++) {
            $codes[$i] = \DB::select('SELECT c.code FROM classrooms c INNER JOIN exams e ON c.id=e.classroom_id WHERE e.id=?;',
                [$exams[$i]->classroom_id])[0]->code;
        }

        $j = 0;
        for ($i=0; $i<count($exams); $i++) {
            if ($i == $real_values[$j]) {
                $codes[$i-1] .= ', ' . $codes[$i];
                //unset($codes[$i]);
                error_log('j : ' . $j);
                if (count($real_values)-1 != $j) {
                    $j++;
                }
            }
            // error_log($codes[$i]);
        }
        for ($i=0; $i<count($real_values); $i++) {
            unset($exams[$real_values[$i]]);
        }

		return view('home', compact('title', 'exams', 'codes'));
	}

}

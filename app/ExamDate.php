<?php namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon\Carbon;

class ExamDate extends Model {

    use SoftDeletes;

    protected $table = 'exam_dates';

    public $timestamps = false;

    protected $fillable = [
        'title',
        'begin',
        'end',
    ];

    protected $dates = ['begin', 'end', 'deleted_at'];

    /**
     * @param $query
     * @return mixed
     */
    public function scopeNearest($query)
    {
        return $query->where('begin', '>', Carbon::now())->orderBy('begin', 'asc')->take(1);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function exams()
    {
        return $this->hasMany('App\Exam');
    }

}

<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Exam extends Model {

    protected $table = 'exams';

    public $timestamps = false;

    protected $fillable = [
        'exam_dates_id',
        'course_id',
        'start_time',
        'end_time',
        'classroom_id',
    ];

    /*public function instructors()
    {
        return $this->belongsToMany('App\Instructor');
    }*/

    public function classrooms()
    {
        return $this->hasMany('App\Classroom');
    }

    public function course()
    {
        return $this->belongsTo('App\Course');
    }

    /**
     * @param $query
     * @return mixed
     */
    public function scopeVize($query)
    {
        return $query->where('exam_dates_id', '=', 1)->orderBy('start_time', 'asc');
    }

}

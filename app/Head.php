<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Head extends Model {

    protected $table = 'heads';

    public $timestamps = false;

    protected $fillable = [
        'instructor_id',
        'department_id',
    ];

    public function department()
    {
        return $this->belongsTo('App\Department');
    }

}

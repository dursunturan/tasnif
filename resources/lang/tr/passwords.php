<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Password Reminder Language Lines
	|--------------------------------------------------------------------------
	|
	| The following language lines are the default lines which match reasons
	| that are given by the password broker for a password update attempt
	| has failed, such as for an invalid token or invalid new password.
	|
	*/

	"password" => "Şifre en az 6 karakterden oluşmalıdır.",
	"user" => "Bu e-posta adresi ile kayıtlı bir kullanıcı mevcut değil.",
	"token" => "Yanlış şifre sıfırlama anahtarı girdiniz.",
	"sent" => "E-posta adresinize şifre sıfırlama bilgileri gönderildi!",
	"reset" => "Şifreniz sıfırlandı!",

];

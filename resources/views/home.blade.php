@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">{{ $title }} Sınav Programı</div>
				<div class="panel-body">
				@foreach($exams as $key=>$exam)
                    {{ $exam->start_time . ' - ' . $exam->end_time }} ::: {{ $exam->course->title . '::' . $codes[$key]}} <br>
				@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Yönetim Paneli</div>
                <!--
                    Eğer soft-delete edilmemiş hiçbir kayıt yok ExamDate'de Sınav Dönemi Oluştur yazsın
                    Var ise progress belirten bi çubuk olsun ve yanında Derslerin kaçının tarihinin belirtildiği yüzde olarak belirtilsin
                    ve de Sınav Dönemi Kapat aktif olsun. Sınav Dönemi Kapat eğer %100 değilken seçilirse, uyarı verilsin.
                    silinirse ExamDate soft-delete yapılsın ve sınav yerlerini belirten pdfler bölümlere göre üretilsin.
                -->
				<div class="panel-body">
                    <li><a href="{{ url('/admin/setup/step-1-exam-plan') }}">Step 1: Exam Plan</a></li>
                    <li><a href="{{ url('/admin/setup/step-2-exam-dates') }}">Step 2: Exam Dates</a></li>
                    <li><a href="{{ url('/admin/setup/step-3-departments') }}">Step 3: Departments</a></li>
                    <li><a href="{{ url('/admin/setup/step-4-classrooms') }}">Step 4: Classrooms</a></li>
                    <li><a href="{{ url('/admin/setup/step-5-instructors') }}">Step 5: Instructors</a></li>
                    <li><a href="{{ url('/admin/setup/step-6-courses') }}">Step 6: Courses</a></li>
                    <li><a href="{{ url('/admin/setup/step-7-heads') }}">Step 7: Heads</a></li>
                    <li><a href="{{ url('/admin/setup/step-8-confirm') }}">Step 8: Confirm</a></li>
		        </div>
		    </div>
	    </div>
    </div>
</div>
@endsection
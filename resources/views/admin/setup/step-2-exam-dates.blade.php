@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Sınav Tarihleri</div>
				<div class="panel-body">
                    {!! Form::open(array('class'=>'form_horizontal', 'id'=>'form_2')) !!}
                    @foreach(Session::get('exams') as $exam)
                    <div class="form-group has-feedback">
                        <label class="control-label" for="{{ $exam }}">{{ $exam }} Tarihi</label>
                        <div class="input-group">
                            <span class="input-group-addon glyphicon glyphicon-calendar"></span>
                            <input class="form-control" placeholder="{{ $exam }} Başlangıç" id="{{ strtolower($exam).'_begin' }}" name="{{ strtolower($exam).'_begin' }}" type="text" value="">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon glyphicon glyphicon-calendar"></span>
                            <input class="form-control" placeholder="{{ $exam }} Bitiş" id="{{ strtolower($exam).'_end' }}" name="{{ strtolower($exam).'_end' }}" type="text" value="">
                        </div>
                    </div>
                    @endforeach
                    <div class="form-group">
                        <p>
                        <a class="btn btn-lg btn-primary pull-right btnNext" role="button">Sonraki Adım</a>
                        </p>
                    </div>
                    {!! Form::close() !!}
		        </div>
		    </div>
	    </div>
    </div>
</div>
@endsection
@section('footer')
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
      <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
      <script src="//raw.githubusercontent.com/jquery/jquery-ui/master/ui/i18n/datepicker-tr.js"></script>
      <script src="//cdn.jsdelivr.net/jquery.validation/1.13.1/jquery.validate.js"></script>
      <script src="//cdn.jsdelivr.net/jquery.validation/1.13.1/additional-methods.js"></script>
      <script src="//raw.githubusercontent.com/jzaefferer/jquery-validation/master/src/localization/messages_tr.js"></script>
      <script>
      $(function() {
        // Convert String to Date
        var begin = "{{ Session::get('begin') }}";
        var begin_day = begin.split('/')[0],
            begin_month = begin.split('/')[1],
            begin_year = begin.split('/')[2];
        var begin_date = new Date(begin_year, begin_month, begin_day);
        console.log(begin_date);
        var end = "{{ Session::get('end') }}";
        var end_day = end.split('/')[0],
            end_month = end.split('/')[1],
            end_year = end.split('/')[2];
        var end_date = new Date(end_year, end_month, end_day);
        console.log(begin_date + ': ' + end_date);
        // Default minDate rules
        var thirty_days_since_begin_date = new Date(begin_date.getTime() + 30 * 86400000);
        var thirtyfive_days_since_begin_date = new Date(begin_date.getTime() + 35 * 86400000);
        var sixtyfive_days_since_begin_date = new Date(begin_date.getTime() + 65 * 86400000);
        var seventy_days_since_begin_date = new Date(begin_date.getTime() + 70 * 86400000);
        var eighty_days_since_begin_date = new Date(begin_date.getTime() + 80 * 86400000);
        var eightyfour_days_since_begin_date = new Date(begin_date.getTime() + 84 * 86400000);
        // Default maxDate rules
        var fiftyfive_days_before_end_date = new Date(end_date.getTime() - 55 * 86400000);
        var fifty_days_before_end_date = new Date(end_date.getTime() - 50 * 86400000);
        var twenty_days_before_end_date = new Date(end_date.getTime() - 20 * 86400000);
        var fifteen_days_before_end_date = new Date(end_date.getTime() - 15 * 86400000);
        var five_days_before_end_date = new Date(end_date.getTime() - 5 * 86400000);
        var one_day_before_end_date = new Date(end_date.getTime() - 1 * 86400000);
        //console.log(thirty_days_since_begin_date + ': ' + fiftyfive_days_before_end_date);

        $('#vize_begin').datepicker({
            showMonthAfterYear: false,
            hideIfNoPrevNext: true,
            dateFormat: "dd/mm/yy",
            minDate: thirty_days_since_begin_date,
            maxDate: fiftyfive_days_before_end_date,
            onSelect: function(dateText, inst) {
                // removed
                //var toDateForVizeEnd = new Date(inst.selectedYear, inst.selectedMonth, parseInt(inst.selectedDay)+7);
                //$('#vize_end').datepicker('option', 'minDate', toDateForVizeEnd);
            }
        },
            $.datepicker.regional['tr']
        );
        $('#vize_end').datepicker({
            showMonthAfterYear: false,
            hideIfNoPrevNext: true,
            dateFormat: "dd/mm/yy",
            minDate: thirtyfive_days_since_begin_date,
            maxDate: fifty_days_before_end_date,
            onSelect: function(dateText, inst) {
                // removed
            }
        },
            $.datepicker.regional['tr']
        );
        $('#final_begin').datepicker({
            showMonthAfterYear: false,
            hideIfNoPrevNext: true,
            dateFormat: "dd/mm/yy",
            minDate: sixtyfive_days_since_begin_date,
            maxDate: twenty_days_before_end_date,
            onSelect: function(dateText, inst) {
                // removed
            }
        },
            $.datepicker.regional['tr']
        );
        $('#final_end').datepicker({
            showMonthAfterYear: false,
            hideIfNoPrevNext: true,
            dateFormat: "dd/mm/yy",
            minDate: seventy_days_since_begin_date,
            maxDate: fifteen_days_before_end_date,
            onSelect: function(dateText, inst) {
                // removed
            }
        },
            $.datepicker.regional['tr']
        );
        $('#büt_begin').datepicker({
            showMonthAfterYear: false,
            hideIfNoPrevNext: true,
            dateFormat: "dd/mm/yy",
            minDate: eighty_days_since_begin_date,
            maxDate: five_days_before_end_date,
            onSelect: function(dateText, inst) {
                // removed
            }
        },
            $.datepicker.regional['tr']
        );
        $('#büt_end').datepicker({
            showMonthAfterYear: false,
            hideIfNoPrevNext: true,
            dateFormat: "dd/mm/yy",
            minDate: eightyfour_days_since_begin_date,
            maxDate: one_day_before_end_date,
            onSelect: function(dateText, inst) {
                // removed
            }
        },
            $.datepicker.regional['tr']
        );
        $('.btnNext').click(function() {
            console.log($('#form_2').valid());
            if ($('#form_2').valid()) {
                $.ajaxSetup(
                {
                    type: 'post',
                    url: 'ajax',
                    headers: {
                        'X-CSRF-Token': $('input[name="_token"]').val()
                    },
                    dataType: 'json'
                });
                $.post('ajax', {
                    'vize_begin': $('#vize_begin').val(),
                    'vize_end': $('#vize_end').val(),
                    'final_begin': $('#final_begin').val(),
                    'final_end': $('#final_end').val(),
                    'but_begin': $('#büt_begin').val(),
                    'but_end': $('#büt_end').val()
                }, function (data) {
                    console.log(data);
                    if (data == true) {
                        setTimeout(function () {
                            window.location.href = "step-3-departments";
                        }, 1000);
                    };
                });
            }
        });
        $.validator.addMethod("dateFormat",
            function(value, e) {
                var check = false;
                var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                if(re.test(value)){
                    var adata = value.split('/');
                    var dd = parseInt(adata[0],10);
                    var mm = parseInt(adata[1],10);
                    var yyyy = parseInt(adata[2],10);
                    var xdata = new Date(yyyy,mm-1,dd);
                    if ( ( xdata.getFullYear() == yyyy ) && ( xdata.getMonth () == mm - 1 ) && ( xdata.getDate() == dd ) )
                        check = true;
                    else
                        check = false;
                } else
                    check = false;
                return this.optional(e) || check;
            },
            "Tarih gün/ay/yıl şeklinde olmalıdır."
        );
        $('#form_2').validate({
            rules: {
                vize_begin: {
                    required: true,
                    dateFormat: true
                },
                vize_end: {
                    required: true,
                    dateFormat: true
                },
                final_begin: {
                    required: true,
                    dateFormat: true
                },
                final_end: {
                    required: true,
                    dateFormat: true
                },
                büt_begin: {
                    required: true,
                    dateFormat: true
                },
                büt_end: {
                    required: true,
                    dateFormat: true
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
      });
      </script>
@endsection
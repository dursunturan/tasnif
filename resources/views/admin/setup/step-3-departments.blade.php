@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Bölümler</div>
				<div class="panel-body">
                    {!! Form::open(array('class'=>'form_horizontal', 'id'=>'form_3')) !!}
                    <div class="form-group has-feedback">
                        {!! Form::label('department_list', 'Bölümler') !!}
                        {!! Form::select('department_list[]',
                        ['Bilgisayar Mühendisliği', 'Çevre Mühendisliği', 'Elektrik Elektronik Mühendisliği',
                        'Endüstri Mühendisliği', 'Gıda Mühendisliği', 'Harita Mühendisliği', 'İnşaat Mühendisliği',
                        'Kimya Mühendisliği', 'Makina Mühendisliği', 'Malzeme Bilimi ve Mühendisliği'],
                        null, ['id' => 'department_list', 'class' => 'form-control', 'multiple']) !!}
                    </div>
                    <div class="form-group">
                        <p>
                        <a class="btn btn-lg btn-primary pull-right btnNext" role="button">Sonraki Adım</a>
                        </p>
                    </div>
                    {!! Form::close() !!}
		        </div>
		    </div>
	    </div>
    </div>
</div>
@endsection
@section('footer')
<script src="//cdnjs.cloudflare.com/ajax/libs/select2/4.0.0/js/select2.min.js"></script>
<script>
$(function() {
    $('#department_list').select2({
        placeholder: 'Eklemek istediğiniz bölümü yazın',
        tags: true
    });
    $('.btnNext').click(function() {
        var departments = [];
        $('#department_list :selected').each(function() {
          departments.push($(this).text());
        });
        console.log(departments);
        $.ajaxSetup(
        {
            type: 'post',
            url: 'ajax',
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            dataType: 'json'
        });
        $.post('ajax', {
            'departments': departments
        }, function (data) {
            console.log(data);
            if (data == true) {
                setTimeout(function () {
                    window.location.href = "step-4-classrooms";
                }, 1000);
            };
        });
    });
});
</script>
@endsection
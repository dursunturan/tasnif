@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Eğitmenler</div>
				<div class="panel-body">
                {!! Form::open(array('class'=>'form_horizontal', 'id'=>'form_5')) !!}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-2">
                                <input type="text" class="form-control" name="instructors[0][full_name]" placeholder="Ad-Soyad" />
                            </div>
                            <div class="col-xs-2">
                                <input type="text" class="form-control" name="instructors[0][email]" placeholder="E-Posta" />
                            </div>
                            <div class="col-xs-2">
                                {!! Form::select('instructors[0][degree]',
                                [1=>'Okutman', 'Araştırma Görevlisi', 'Öğretim Görevlisi',
                                'Yardımcı Doçent', 'Doçent', 'Profesör'],
                                null, ['id' => 'degree', 'class' => 'form-control']) !!}
                            </div>
                            <div class="col-xs-1">
                                <input type="text" class="form-control" name="instructors[0][extension_number]" placeholder="Dahili Telefon" />
                            </div>
                            <div class="col-xs-2">
                                <input type="text" class="form-control" name="instructors[0][personal_phone_number]" placeholder="Kişisel Telefon" />
                            </div>
                            <div class="col-xs-2">
                                <select id="department" class="form-control" name="instructors[0][department]">
                                <?php $i=1; ?>
                                @foreach(Session::get('departments') as $department)
                                    <?php
                                        echo "<option value=\"$i\">$department</option>";
                                        $i++;
                                    ?>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-xs-1">
                                <button type="button" class="btn btn-success btn-number btnAdd">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- Template -->
                    <div class="form-group hide" id="form_template">
                        <div class="row">
                            <div class="col-xs-2">
                                <input type="text" class="form-control" name="full_name" placeholder="Ad-Soyad" />
                            </div>
                            <div class="col-xs-2">
                                <input type="text" class="form-control" name="email" placeholder="E-Posta" />
                            </div>
                            <div class="col-xs-2">
                                {!! Form::select('degree',
                                [1=>'Okutman', 'Araştırma Görevlisi', 'Öğretim Görevlisi',
                                'Yardımcı Doçent', 'Doçent', 'Profesör'],
                                null, ['id' => 'degree', 'class' => 'form-control']) !!}
                            </div>
                            <div class="col-xs-1">
                                <input type="text" class="form-control" name="extension_number" placeholder="Dahili Telefon" />
                            </div>
                            <div class="col-xs-2">
                                <input type="text" class="form-control" name="personal_phone_number" placeholder="Kişisel Telefon" />
                            </div>
                            <div class="col-xs-2">
                                <select id="department" class="form-control" name="department">
                                <?php $i=1; ?>
                                @foreach(Session::get('departments') as $department)
                                    <?php
                                        echo "<option value=\"$i\">$department</option>";
                                        $i++;
                                    ?>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-xs-1">
                                <button type="button" class="btn btn-danger btn-number btnDelete">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <p>
                        <a class="btn btn-lg btn-primary pull-right btnNext" role="button">Sonraki Adım</a>
                        </p>
                    </div>
                {!! Form::close() !!}
		        </div>
		    </div>
	    </div>
    </div>
</div>
@endsection
@section('footer')
<script>
$(function() {
    var instructor_index = 0;
    $('.btnNext').click(function() {
        var instructors_full_name = new Array(instructor_index+1);
        var instructors_email = new Array(instructor_index+1);
        var instructors_degree_id = new Array(instructor_index+1);
        var instructors_extension_number = new Array(instructor_index+1);
        var instructors_personal_phone_number = new Array(instructor_index+1);
        var instructors_department_id = new Array(instructor_index+1);
        var fields = $( ".form-control" ).serializeArray();
        var pattern = new RegExp(/instructors\[\d+\]\[[^\s]+\]/);
        $.each(fields, function(i, field) {
            //console.log(pattern.test(field.name) + ": ", field.name);
            if (field.value != '' && pattern.test(field.name)) {
                var row = parseInt(i/6);
                var column = i%6;
                switch (column) {
                    case 0:
                        instructors_full_name[row] = field.value;
                        break;
                    case 1:
                        instructors_email[row] = field.value;
                        break;
                    case 2:
                        instructors_degree_id[row] = field.value;
                        break;
                    case 3:
                        instructors_extension_number[row] = field.value;
                        break;
                    case 4:
                        instructors_personal_phone_number[row] = field.value;
                        break;
                    case 5:
                        instructors_department_id[row] = field.value;
                        break;
                    default:
                        console.log("problem occured");
                        break;
                }
            }
        });
        console.log(instructors_full_name);
        console.log(instructors_email);
        console.log(instructors_degree_id);
        console.log(instructors_extension_number);
        console.log(instructors_personal_phone_number);
        console.log(instructors_department_id);
        $.ajaxSetup(
        {
            type: 'post',
            url: 'ajax',
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            dataType: 'json'
        });
        $.post('ajax', {
            'instructors_full_name': instructors_full_name,
            'instructors_email': instructors_email,
            'instructors_degree_id': instructors_degree_id,
            'instructors_extension_number': instructors_extension_number,
            'instructors_personal_phone_number': instructors_personal_phone_number,
            'instructors_department_id': instructors_department_id
        }, function (data) {
            console.log(data);
            if (data == true) {
                setTimeout(function () {
                    window.location.href = "step-6-courses";
                }, 1000);
            };
        });
    });
    $('.btnAdd').click(function() {
        instructor_index++;
        var $template = $('#form_template'),
        $clone    = $template
                    .clone()
                    .removeClass('hide')
                    .removeAttr('id')
                    .attr('data-instructor-index', instructor_index)
                    .insertBefore($template);
        $clone
        .find('[name="full_name"]').attr('name', 'instructors[' + instructor_index + '][full_name]').end()
        .find('[name="email"]').attr('name', 'instructors[' + instructor_index + '][email]').end()
        .find('[name="degree"]').attr('name', 'instructors[' + instructor_index + '][degree]').end()
        .find('[name="extension_number"]').attr('name', 'instructors[' + instructor_index + '][extension_number]').end()
        .find('[name="personal_phone_number"]').attr('name', 'instructors[' + instructor_index + '][personal_phone_number]').end()
        .find('[name="department"]').attr('name', 'instructors[' + instructor_index + '][department]').end();
    });
    $('#form_5').on('click', '.btnDelete', function() {
        var $row  = $(this).parents('.form-group'),
            index = $row.attr('data-instructor-index');
            console.log(index);
        $row.remove();
        instructor_index--;
    });
});
</script>
@endsection
@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Onay</div>
				<div class="panel-body">
                {!! Form::open(array('url'=> 'admin', 'class'=>'form_horizontal', 'id'=>'form_8')) !!}
                    Yaptığınız ayarlamaları onaylıyor musunuz? Cevabınız evet ise "Onaylıyorum" butonuna basınız.<br>
                    Hayır ise buraya tıklayınız.
                    <div class="form-group">
                        <p>
                        {!! Form::submit('Onaylıyorum', array('class'=>'btn btn-lg btn-default')) !!}
                        </p>
                    </div>
                {!! Form::close() !!}
		        </div>
		    </div>
	    </div>
    </div>
</div>
@endsection
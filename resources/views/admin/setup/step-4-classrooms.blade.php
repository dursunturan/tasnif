@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Sınıflar</div>
				<div class="panel-body">
                {!! Form::open(array('class'=>'form_horizontal', 'id'=>'form_4')) !!}
                    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-4">
                                <input type="text" class="form-control" name="classrooms[0][code]" placeholder="Sınıf Kodu" />
                            </div>
                            <div class="col-xs-2">
                                <input type="number" min="10" step="10" class="form-control" name="classrooms[0][capacity]" placeholder="Kapasitesi" />
                            </div>
                            <div class="col-xs-1">
                                <button type="button" class="btn btn-success btn-number btnAdd">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- Template -->
                    <div class="form-group hide" id="form_template">
                        <div class="row">
                            <div class="col-xs-4">
                                <input type="text" class="form-control" name="code" placeholder="Sınıf Kodu" />
                            </div>
                            <div class="col-xs-2">
                                <input type="number" class="form-control" name="capacity" placeholder="Kapasitesi" />
                            </div>
                            <div class="col-xs-1">
                                <button type="button" class="btn btn-danger btn-number btnDelete">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <p>
                        <a class="btn btn-lg btn-primary pull-right btnNext" role="button">Sonraki Adım</a>
                        </p>
                    </div>
                {!! Form::close() !!}
		        </div>
		    </div>
	    </div>
    </div>
</div>
@endsection
@section('footer')
<script>
$(function() {
    var classroom_index = 0;
    $('.btnNext').click(function() {
        var classrooms_code = new Array(classroom_index+1);
        var classrooms_capacity = new Array(classroom_index+1);
        var fields = $( ".form-control" ).serializeArray();
        $.each(fields, function(i, field) {
            if (field.value != '') {
                console.log(i + ":" + field.value);
                var row = parseInt(i/2);
                var column = i%2;
                if (column == 0) {
                    classrooms_code[row] = field.value;
                } else {
                    classrooms_capacity[row] = field.value;
                }
            }
        });
        console.log(classrooms_code);
        console.log(classrooms_capacity);
        $.ajaxSetup(
        {
            type: 'post',
            url: 'ajax',
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            dataType: 'json'
        });
        $.post('ajax', {
            'classrooms_code': classrooms_code,
            'classrooms_capacity': classrooms_capacity
        }, function (data) {
            console.log(data);
            if (data == true) {
                setTimeout(function () {
                    window.location.href = "step-5-instructors";
                }, 1000);
            };
        });
    });
    $('.btnAdd').click(function() {
        classroom_index++;
        var $template = $('#form_template'),
        $clone    = $template
                    .clone()
                    .removeClass('hide')
                    .removeAttr('id')
                    .attr('data-classroom-index', classroom_index)
                    .insertBefore($template);
        $clone
        .find('[name="code"]').attr('name', 'classrooms[' + classroom_index + '][code]').end()
        .find('[name="capacity"]').attr('name', 'classrooms[' + classroom_index + '][capacity]').end();
    });
    $('#form_4').on('click', '.btnDelete', function() {
        var $row  = $(this).parents('.form-group'),
            index = $row.attr('data-classroom-index');
            console.log(index);
        $row.remove();
        classroom_index--;
    });
});
</script>
@endsection
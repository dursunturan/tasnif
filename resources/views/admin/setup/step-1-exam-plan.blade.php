@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Dönem Sınav Düzeni</div>
				<div class="panel-body">
                    {!! Form::open(array('class'=>'form_horizontal', 'id'=>'form_1')) !!}
                    <div class="form-group has-feedback">
                        <label class="control-label" for="begin">Başlangıç Tarihi</label>
                        <div class="input-group">
                            <span class="input-group-addon glyphicon glyphicon-calendar"></span>
                            {!! Form::text('begin', '', array('class'=>'form-control','placeholder'=>'Dönem Başlangıç', 'id' => 'begin')) !!}
                        </div>
                    </div>
                    <div class="form-group has-feedback">
                        <label class="control-label" for="end">Bitiş Tarihi</label>
                        <div class="input-group">
                            <span class="input-group-addon glyphicon glyphicon-calendar"></span>
                            {!! Form::text('end', '', array('class'=>'form-control','placeholder'=>'Dönem Bitiş', 'id' => 'end')) !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label" for="exams[]">Sınav Düzeni</label>
                        <div class="input-group">
                          {!! Form::checkbox('exams[]', 'Vize', true, array('disabled')) !!} Vize
                          {!! Form::checkbox('exams[]', 'Final', true, array('disabled')) !!} Final
                          {!! Form::checkbox('exams[]', 'Büt', false) !!} Büt
                        </div>
                    </div>
                    <div class="form-group">
                        <p>
                        <a class="btn btn-lg btn-primary pull-right btnNext" role="button">Sonraki Adım</a>
                        </p>
                    </div>
                    {!! Form::close() !!}
                </div>
		    </div>
		    </div>
	    </div>
    </div>
</div>
@endsection
@section('footer')
      <script src="//code.jquery.com/jquery-1.10.2.js"></script>
      <script src="//code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
      <script src="//raw.githubusercontent.com/jquery/jquery-ui/master/ui/i18n/datepicker-tr.js"></script>
      <script src="//cdn.jsdelivr.net/jquery.validation/1.13.1/jquery.validate.js"></script>
      <script src="//cdn.jsdelivr.net/jquery.validation/1.13.1/additional-methods.js"></script>
      <script src="//raw.githubusercontent.com/jzaefferer/jquery-validation/master/src/localization/messages_tr.js"></script>
      <script>
      $(function() {
        $('#begin').datepicker({
            showMonthAfterYear: false,
            hideIfNoPrevNext: true,
            dateFormat: "dd/mm/yy",
            minDate: 0,
            onSelect: function(dateText, inst) {
                var toDate = new Date(inst.selectedYear, inst.selectedMonth, parseInt(inst.selectedDay)+1);
                $('#end').datepicker('option', 'minDate', toDate);
            }
        },
            $.datepicker.regional['tr']
        );
        $('#end').datepicker({
            showMonthAfterYear: false,
            hideIfNoPrevNext: true,
            dateFormat: "dd/mm/yy",
            minDate: 1,
            onSelect: function(dateText, inst) {
                var toDate = new Date(inst.selectedYear, inst.selectedMonth, parseInt(inst.selectedDay)-1);
                $('#begin').datepicker('option', 'maxDate', toDate);
            }
        },
            $.datepicker.regional['tr']
        );
        $('.btnNext').click(function() {
            console.log($('#begin').val());
            console.log($('#end').val());
            var exams = [];
            $("input[name='exams[]']:checked").each(function(){exams.push($(this).val());});
            console.log(exams);
            console.log($('#form_1').valid());
            if ($('#form_1').valid()) {
                $.ajaxSetup(
                {
                    type: 'post',
                    url: 'ajax',
                    headers: {
                        'X-CSRF-Token': $('input[name="_token"]').val()
                    },
                    dataType: 'json'
                });
                $.post('ajax', {
                    'begin': $('#begin').val(),
                    'end': $('#end').val(),
                    'exams': exams
                }, function (data) {
                    console.log(data);
                    if (data == true) {
                        setTimeout(function () {
                            window.location.href = "step-2-exam-dates";
                        }, 1000);
                    };
                });
            }
        });
        $.validator.addMethod("dateFormat",
            function(value, e) {
                var check = false;
                var re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
                if(re.test(value)){
                    var adata = value.split('/');
                    var dd = parseInt(adata[0],10);
                    var mm = parseInt(adata[1],10);
                    var yyyy = parseInt(adata[2],10);
                    var xdata = new Date(yyyy,mm-1,dd);
                    if ( ( xdata.getFullYear() == yyyy ) && ( xdata.getMonth () == mm - 1 ) && ( xdata.getDate() == dd ) )
                        check = true;
                    else
                        check = false;
                } else
                    check = false;
                return this.optional(e) || check;
            },
            "Tarih gün/ay/yıl şeklinde olmalıdır."
        );
        $('#form_1').validate({
            rules: {
                begin: {
                    required: true,
                    dateFormat: true
                },
                end: {
                    required: true,
                    dateFormat: true
                }
            },
            highlight: function(element) {
                $(element).closest('.form-group').addClass('has-error');
            },
            unhighlight: function(element) {
                $(element).closest('.form-group').removeClass('has-error');
            },
            errorElement: 'span',
            errorClass: 'help-block',
            errorPlacement: function(error, element) {
                if(element.parent('.input-group').length) {
                    error.insertAfter(element.parent());
                } else {
                    error.insertAfter(element);
                }
            }
        });
      });
      </script>
@endsection
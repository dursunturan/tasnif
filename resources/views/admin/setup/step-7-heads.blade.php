@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Bölüm Başkanları</div>
				<div class="panel-body">
                    {!! Form::open(array('class'=>'form_horizontal', 'id'=>'form_7')) !!}
                    @foreach(Session::get('departments') as $id => $department)
                    <div class="form-group has-feedback">
                        <label class="control-label" for="head[{{ $id }}]">{{ $department }} Bölüm Başkanı</label>
                        <div class="input-group">
                            <select id="head[{{ $id+1 }}]" class="form-control" name="{{ $id+1 }}">
                        @foreach(Session::get('instructors_department_id') as $instructor_id => $instructor_department_id)
                            @if($instructor_department_id-1 == $id)
                                {!! '<option value="' . ($instructor_id+2) . '">'. Session::get('instructors_full_name.' . $instructor_id) . '</option>' !!}
                            @endif
                        @endforeach
                            </select>
                        </div>
                    </div>
                    @endforeach
                    <div class="form-group">
                        <p>
                        <a class="btn btn-lg btn-primary pull-right btnNext" role="button">Sonraki Adım</a>
                        </p>
                    </div>
                    {!! Form::close() !!}
		        </div>
		    </div>
	    </div>
    </div>
</div>
@endsection
@section('footer')
<script>
$(function() {
    $('.btnNext').click(function() {
        var heads_department_id = [];
        var heads_instructor_id = [];
        var fields = $( ".form-control" ).serializeArray();
        // console.log(fields);
        $.each(fields, function(i, field) {
            // console.log(i + ':' + field.name + ' ' + field.value);
            heads_department_id.push(field.name);
            heads_instructor_id.push(field.value);
        });
        // console.log(heads_department_id + '-' + heads_instructor_id);
        $.ajaxSetup(
        {
            type: 'post',
            url: 'ajax',
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            dataType: 'json'
        });
        $.post('ajax', {
            'heads_department_id': heads_department_id,
            'heads_instructor_id': heads_instructor_id
        }, function (data) {
            console.log(data);
            if (data == true) {
                setTimeout(function () {
                    window.location.href = "step-8-confirm";
                }, 1000);
            };
        });
    });
});
</script>
@endsection
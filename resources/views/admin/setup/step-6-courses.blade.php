@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Dersler</div>
				<div class="panel-body">
				{!! Form::open(array('class'=>'form_horizontal', 'id'=>'form_6')) !!}
				    <div class="form-group">
                        <div class="row">
                            <div class="col-xs-3">
                                <input type="text" class="form-control" name="courses[0][title]" placeholder="Ders" />
                            </div>
                            <div class="col-xs-2">
                                <input type="text" class="form-control" name="courses[0][quota]" placeholder="Kontenjan" />
                            </div>
                            <div class="col-xs-3">
                                <select id="department" class="form-control" name="courses[0][department]">
                                <?php $i=1; ?>
                                @foreach(Session::get('departments') as $department)
                                    <?php
                                        echo "<option value=\"$i\">$department</option>";
                                        $i++;
                                    ?>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-xs-3">
                                <select id="instructor" class="form-control" name="courses[0][instructor]">
                                <?php $i=2; ?>
                                @foreach(Session::get('instructors_full_name') as $instructor)
                                    <?php
                                        echo "<option value=\"$i\">$instructor</option>";
                                        $i++;
                                    ?>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-xs-1">
                                <button type="button" class="btn btn-success btn-number btnAdd">
                                    <span class="glyphicon glyphicon-plus"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <!-- Template -->
                    <div class="form-group hide" id="form_template">
                        <div class="row">
                            <div class="col-xs-3">
                                <input type="text" class="form-control" name="title" placeholder="Ders" />
                            </div>
                            <div class="col-xs-2">
                                <input type="text" class="form-control" name="quota" placeholder="Kontenjan" />
                            </div>
                            <div class="col-xs-3">
                                <select id="department" class="form-control" name="department">
                                <?php $i=1; ?>
                                @foreach(Session::get('departments') as $department)
                                    <?php
                                        echo "<option value=\"$i\">$department</option>";
                                        $i++;
                                    ?>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-xs-3">
                                <select id="instructor" class="form-control" name="instructor">
                                <?php $i=2; ?>
                                @foreach(Session::get('instructors_full_name') as $instructor)
                                    <?php
                                        echo "<option value=\"$i\">$instructor</option>";
                                        $i++;
                                    ?>
                                @endforeach
                                </select>
                            </div>
                            <div class="col-xs-1">
                                <button type="button" class="btn btn-danger btn-number btnDelete">
                                    <span class="glyphicon glyphicon-minus"></span>
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <p>
                        <a class="btn btn-lg btn-primary pull-right btnNext" role="button">Sonraki Adım</a>
                        </p>
                    </div>
				{!! Form::close() !!}
		        </div>
		    </div>
	    </div>
    </div>
</div>
@endsection
@section('footer')
<script>
$(function() {
    var course_index = 0;
    $('.btnNext').click(function() {
        var courses_title = new Array(course_index+1);
        var courses_quota = new Array(course_index+1);
        var courses_department_id = new Array(course_index+1);
        var courses_instructor_id = new Array(course_index+1);
        var fields = $( ".form-control" ).serializeArray();
        var pattern = new RegExp(/courses\[\d+\]\[[^\s]+\]/);
        $.each(fields, function(i, field) {
            //console.log(pattern.test(field.name) + ": ", field.name);
            if (field.value != '' && pattern.test(field.name)) {
                var row = parseInt(i/4);
                var column = i%4;
                switch (column) {
                    case 0:
                        courses_title[row] = field.value;
                        break;
                    case 1:
                        courses_quota[row] = field.value;
                        break;
                    case 2:
                        courses_department_id[row] = field.value;
                        break;
                    case 3:
                        courses_instructor_id[row] = field.value;
                        break;
                    default:
                        console.log("problem occured");
                        break;
                }
            }
        });
        console.log(courses_title);
        console.log(courses_quota);
        console.log(courses_department_id);
        console.log(courses_instructor_id);
        $.ajaxSetup(
        {
            type: 'post',
            url: 'ajax',
            headers: {
                'X-CSRF-Token': $('input[name="_token"]').val()
            },
            dataType: 'json'
        });
        $.post('ajax', {
            'courses_title': courses_title,
            'courses_quota': courses_quota,
            'courses_department_id': courses_department_id,
            'courses_instructor_id': courses_instructor_id
        }, function (data) {
            console.log(data);
            if (data == true) {
                setTimeout(function () {
                    window.location.href = "step-7-heads";
                }, 1000);
            };
        });
    });
    $('.btnAdd').click(function() {
        course_index++;
        var $template = $('#form_template'),
        $clone    = $template
                    .clone()
                    .removeClass('hide')
                    .removeAttr('id')
                    .attr('data-course-index', course_index)
                    .insertBefore($template);
        $clone
        .find('[name="title"]').attr('name', 'courses[' + course_index + '][title]').end()
        .find('[name="quota"]').attr('name', 'courses[' + course_index + '][quota]').end()
        .find('[name="department"]').attr('name', 'courses[' + course_index + '][department]').end()
        .find('[name="instructor"]').attr('name', 'courses[' + course_index + '][instructor]').end();
    });
    $('#form_6').on('click', '.btnDelete', function() {
        var $row  = $(this).parents('.form-group'),
            index = $row.attr('data-course-index');
            console.log(index);
        $row.remove();
        course_index--;
    });
});
</script>
@endsection
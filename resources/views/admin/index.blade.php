@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Yönetim Paneli</div>
                <!--
                    Eğer soft-delete edilmemiş hiçbir kayıt yok ExamDate'de Sınav Dönemi Oluştur yazsın
                    Var ise progress belirten bi çubuk olsun ve yanında Derslerin kaçının tarihinin belirtildiği yüzde olarak belirtilsin
                    ve de Sınav Dönemi Kapat aktif olsun. Sınav Dönemi Kapat eğer %100 değilken seçilirse, uyarı verilsin.
                    silinirse ExamDate soft-delete yapılsın ve sınav yerlerini belirten pdfler bölümlere göre üretilsin.
                -->
				<div class="panel-body">
				@if (!isset($nearest_exam))
				    <a class="btn btn-lg btn-primary" role="button" href="{{ url('/admin/setup') }}">Sınav Dönemi Oluştur</a>
				</div>
				@else
                    <button type="button" class="btn btn-lg btn-danger">{{ $nearest_exam->title }} Sınav Dönemini Kapat</button>
                </div>
                @endif
			</div>
		</div>
	</div>
</div>
@endsection
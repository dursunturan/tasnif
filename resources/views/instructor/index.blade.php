@extends('app')

@section('content')
<div class="container">
	<div class="row">
		<div class="col-md-10 col-md-offset-1">
			<div class="panel panel-default">
				<div class="panel-heading">Yönetim Paneli</div>
				<div class="panel-body">
                @foreach($courses as $course)
                <div class="form-group">
                    {{ $course->title . ' ' . $course->department->title }}
                </div>
                @endforeach
                </div>
			</div>
		</div>
	</div>
</div>
@endsection
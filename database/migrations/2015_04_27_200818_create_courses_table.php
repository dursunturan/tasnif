<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCoursesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('courses', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('title');
            $table->integer('quota')->unsigned();
            $table->unsignedInteger('instructor_id');
            $table->unsignedInteger('department_id');
        });

        Schema::table('courses', function($table) {
            $table->foreign('instructor_id')
                ->references('id')->on('instructors');
            $table->foreign('department_id')
                ->references('id')->on('departments');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('courses');
	}

}

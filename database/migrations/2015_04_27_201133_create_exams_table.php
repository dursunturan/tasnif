<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateExamsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('exams', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('exam_dates_id');
            $table->unsignedInteger('course_id');
            $table->dateTime('start_time');
            $table->dateTime('end_time');
            $table->unsignedInteger('classroom_id');
        });

        Schema::table('exams', function($table) {
            $table->foreign('exam_dates_id')
                ->references('id')->on('exam_dates');
            $table->foreign('course_id')
                ->references('id')->on('courses');
            $table->foreign('classroom_id')
                ->references('id')->on('classrooms');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('exams');
	}

}

<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDegreesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('degrees', function(Blueprint $table)
        {
            $table->unsignedInteger('id');
            $table->string('title');
            $table->string('abbr');

            $table->unique('id');
            $table->unique('title');
            $table->unique('abbr');
        });
        // 'Okutman', 'Araştırma Görevlisi', 'Öğretim Görevlisi',
        // 'Yardımcı Doçent', 'Doçent', 'Profesör'
        /*DB::table('degrees')->insert(array(
            array('id' => 0, 'title' => 'Yönetici', 'abbr' => 'Admin'),
            array('id' => 1, 'title' => 'Okutman', 'abbr' => 'Okutman'),
            array('id' => 2, 'title' => 'Araştırma Görevlisi', 'abbr' => 'Arş. Gör.'),
            array('id' => 3, 'title' => 'Öğretim Görevlisi', 'abbr' => 'Öğr. Gör.'),
            array('id' => 4, 'title' => 'Yardımcı Doçent', 'abbr' => 'Yrd. Doç.'),
            array('id' => 5, 'title' => 'Doçent', 'abbr' => 'Doç.'),
            array('id' => 6, 'title' => 'Profesör', 'abbr' => 'Prof.'),
        ));*/
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('degrees');
	}

}

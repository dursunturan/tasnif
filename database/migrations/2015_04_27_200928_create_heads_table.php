<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHeadsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('heads', function(Blueprint $table)
        {
            $table->increments('id');
            $table->unsignedInteger('department_id');
            $table->unsignedInteger('instructor_id');

            $table->unique('department_id');
            $table->unique('instructor_id');
        });

        Schema::table('heads', function($table) {
            $table->foreign('department_id')
                ->references('id')->on('departments');
            $table->foreign('instructor_id')
                ->references('id')->on('instructors');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('heads');
	}

}

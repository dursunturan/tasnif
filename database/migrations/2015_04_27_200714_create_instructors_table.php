<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstructorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('instructors', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('full_name');
            $table->string('email');
            $table->unsignedInteger('degree_id');
            $table->string('extension_number')->nullable();
            $table->string('personal_phone_number')->nullable();
            $table->unsignedInteger('department_id')->nullable();
            $table->string('password', 60);
            $table->rememberToken();
            $table->timestamps();

            $table->unique('email');
        });

        Schema::table('instructors', function($table) {
            $table->foreign('degree_id')
                ->references('id')->on('degrees');
            $table->foreign('department_id')
                ->references('id')->on('departments');
        });

        // One-time password generation for administrator
        /*$random_string = str_random(10);
        $password = Hash::make($random_string);
        $date = new \DateTime;
        DB::table('instructors')->insert(
            array(
                'full_name' => 'admin',
                'email' => 'admin@domain.com',
                'degree_id' => '0',
                'extension_number' => '3359',
                'personal_phone_number' => '05555555555',
                'password' => $password,
                'created_at' => $date,
                'updated_at' => $date,
            )
        );
        error_log($random_string);*/
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('instructors');
	}

}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DepartmentTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('departments')->delete();

        DB::table('departments')->insert(array(
            array('title' => 'Bilgisayar Mühendisliği'),
            array('title' => 'Elektrik Elektronik Mühendisliği'),
        ));

        $this->command->info('Department table seeded!');
	}

}

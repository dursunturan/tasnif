<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ExamTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('exams')->delete();

        DB::table('exams')->insert(array(
            array('exam_dates_id' => 1, 'course_id' => 1,
                'start_time' => '2015-08-04 14:00:00', 'end_time' => '2015-08-04 15:30:00', 'classroom_id' => 1),
            array('exam_dates_id' => 1, 'course_id' => 1,
                'start_time' => '2015-08-04 14:00:00', 'end_time' => '2015-08-04 15:30:00', 'classroom_id' => 5),
            array('exam_dates_id' => 1, 'course_id' => 2,
                'start_time' => '2015-08-04 11:00:00', 'end_time' => '2015-08-04 12:00:00', 'classroom_id' => 2),
            array('exam_dates_id' => 1, 'course_id' => 3,
                'start_time' => '2015-09-04 10:00:00', 'end_time' => '2015-08-04 12:00:00', 'classroom_id' => 3),
            array('exam_dates_id' => 1, 'course_id' => 3,
                'start_time' => '2015-09-04 10:00:00', 'end_time' => '2015-08-04 12:00:00', 'classroom_id' => 4),
            array('exam_dates_id' => 1, 'course_id' => 4,
                'start_time' => '2015-09-04 13:00:00', 'end_time' => '2015-09-04 15:00:00', 'classroom_id' => 3),
            array('exam_dates_id' => 1, 'course_id' => 5,
                'start_time' => '2015-09-04 09:00:00', 'end_time' => '2015-09-04 10:00:00', 'classroom_id' => 2),
            array('exam_dates_id' => 1, 'course_id' => 6,
                'start_time' => '2015-08-04 09:00:00', 'end_time' => '2015-08-04 10:00:00', 'classroom_id' => 7),
            array('exam_dates_id' => 1, 'course_id' => 7,
                'start_time' => '2015-08-04 15:00:00', 'end_time' => '2015-08-04 16:00:00', 'classroom_id' => 7),
            array('exam_dates_id' => 1, 'course_id' => 7,
                'start_time' => '2015-08-04 15:00:00', 'end_time' => '2015-08-04 16:00:00', 'classroom_id' => 8),
            array('exam_dates_id' => 1, 'course_id' => 8,
                'start_time' => '2015-10-04 10:00:00', 'end_time' => '2015-10-04 12:00:00', 'classroom_id' => 10),
            array('exam_dates_id' => 1, 'course_id' => 9,
                'start_time' => '2015-10-04 14:00:00', 'end_time' => '2015-10-04 15:00:00', 'classroom_id' => 10),
            array('exam_dates_id' => 1, 'course_id' => 10,
                'start_time' => '2015-11-04 09:00:00', 'end_time' => '2015-11-04 11:00:00', 'classroom_id' => 6),
            array('exam_dates_id' => 1, 'course_id' => 11,
                'start_time' => '2015-12-04 10:00:00', 'end_time' => '2015-08-04 12:00:00', 'classroom_id' => 10),
            array('exam_dates_id' => 1, 'course_id' => 12,
                'start_time' => '2015-12-04 14:00:00', 'end_time' => '2015-08-04 15:00:00', 'classroom_id' => 8),
        ));

        $this->command->info('Exam table seeded!');
	}

}

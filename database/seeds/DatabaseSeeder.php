<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Model::unguard();

		$this->call('DepartmentTableSeeder');
        $this->call('ExamDateTableSeeder');
        $this->call('ClassroomTableSeeder');
        $this->call('DegreeTableSeeder');
        $this->call('InstructorTableSeeder');
        $this->call('CourseTableSeeder');
        $this->call('HeadTableSeeder');
        $this->call('ExamTableSeeder');
	}

}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ExamDateTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('exam_dates')->delete();

        // yyyy-mm-dd
        DB::table('exam_dates')->insert(array(
            array('title' => 'Vize', 'begin' => '2015-08-03', 'end' => '2015-08-14'),
            array('title' => 'Final', 'begin' => '2015-09-07', 'end' => '2015-09-19'),
            array('title' => 'Büt', 'begin' => '2015-09-22', 'end' => '2015-09-27'),
        ));

        $this->command->info('ExamDate table seeded!');
	}

}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class InstructorTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('instructors')->delete();

        $full_name = ['admin', 'Bünyamin Karabulut', 'Erdal Kılıç', 'Nurettin Şenyer', 'Recai Oktaş',
                    'Erhan Ergün', 'Sedat Akleylek', 'Gökhan Kayhan', 'Kerem Erzurumlu', 'İsmail İşeri',
                    'Güven Önbilgin', 'Okan Özgönenel', 'Çetin Kurnaz', 'İlyas Eminoğlu', 'Muammer Özdemir',
                    'Serap Karagöl'];
        $password = [];
        for ($i=0; $i<count($full_name); $i++) {
            $random_string = str_random(10);
            $password[$i] = \Hash::make($random_string);
            error_log($full_name[$i] . ': ' . $random_string);
        }
        $date = new \DateTime;
        DB::table('instructors')->insert(array(
            array('full_name' => $full_name[0], 'email' => 'admin@domain.com', 'degree_id' => '0',
                'extension_number' => '3359', 'personal_phone_number' => '05555555555',
                'department_id' => null, 'password' => $password[0],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[1], 'email' => 'bunyamin.karabulut@ce.omu.edu.tr', 'degree_id' => '6',
                'extension_number' => '1085', 'personal_phone_number' => '02222222222',
                'department_id' => '1', 'password' => $password[1],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[2], 'email' => 'erdal.kilic@ce.omu.edu.tr', 'degree_id' => '5',
                'extension_number' => '1117', 'personal_phone_number' => '03333333333',
                'department_id' => '1', 'password' => $password[2],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[3], 'email' => 'nurettin.senyer@ce.omu.edu.tr', 'degree_id' => '4',
                'extension_number' => '1097', 'personal_phone_number' => '04444444444',
                'department_id' => '1', 'password' => $password[3],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[4], 'email' => 'roktas@ce.omu.edu.tr', 'degree_id' => '4',
                'extension_number' => '1117', 'personal_phone_number' => '03333333333',
                'department_id' => '1', 'password' => $password[4],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[5], 'email' => 'erhan.ergun@ce.omu.edu.tr', 'degree_id' => '4',
                'extension_number' => '1097', 'personal_phone_number' => '04444444444',
                'department_id' => '1', 'password' => $password[5],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[6], 'email' => 'sedat.akleylek@ce.omu.edu.tr', 'degree_id' => '4',
                'extension_number' => '1117', 'personal_phone_number' => '03333333333',
                'department_id' => '1', 'password' => $password[6],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[7], 'email' => 'gokhan.kayhan@ce.omu.edu.tr', 'degree_id' => '4',
                'extension_number' => '1097', 'personal_phone_number' => '04444444444',
                'department_id' => '1', 'password' => $password[7],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[8], 'email' => 'kerem.erzurumlu@ce.omu.edu.tr', 'degree_id' => '4',
                'extension_number' => '1117', 'personal_phone_number' => '03333333333',
                'department_id' => '1', 'password' => $password[8],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[9], 'email' => 'ismail.iseri@ce.omu.edu.tr', 'degree_id' => '3',
                'extension_number' => '1097', 'personal_phone_number' => '04444444444',
                'department_id' => '1', 'password' => $password[9],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[10], 'email' => 'guven.onbilgin@omu.edu.tr', 'degree_id' => '6',
                'extension_number' => '1097', 'personal_phone_number' => '04444444444',
                'department_id' => '2', 'password' => $password[10],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[11], 'email' => 'okan.ozgonenel@omu.edu.tr', 'degree_id' => '6',
                'extension_number' => '1097', 'personal_phone_number' => '04444444444',
                'department_id' => '2', 'password' => $password[11],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[12], 'email' => 'cetin.kurnaz@omu.edu.tr', 'degree_id' => '4',
                'extension_number' => '1097', 'personal_phone_number' => '04444444444',
                'department_id' => '2', 'password' => $password[12],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[13], 'email' => 'ilyas.eminoglu@omu.edu.tr', 'degree_id' => '4',
                'extension_number' => '1097', 'personal_phone_number' => '04444444444',
                'department_id' => '2', 'password' => $password[13],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[14], 'email' => 'muammer.ozdemir@omu.edu.tr', 'degree_id' => '4',
                'extension_number' => '1097', 'personal_phone_number' => '04444444444',
                'department_id' => '2', 'password' => $password[14],
                'created_at' => $date, 'updated_at' => $date),
            array('full_name' => $full_name[15], 'email' => 'serap.karagol@omu.edu.tr', 'degree_id' => '4',
                'extension_number' => '1097', 'personal_phone_number' => '04444444444',
                'department_id' => '2', 'password' => $password[15],
                'created_at' => $date, 'updated_at' => $date),
        ));

        $this->command->info('Instructor table seeded!');
	}

}

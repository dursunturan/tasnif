<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DegreeTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('degrees')->delete();

        DB::table('degrees')->insert(array(
            array('id' => 0, 'title' => 'Yönetici', 'abbr' => 'Admin'),
            array('id' => 1, 'title' => 'Okutman', 'abbr' => 'Okutman'),
            array('id' => 2, 'title' => 'Araştırma Görevlisi', 'abbr' => 'Arş. Gör.'),
            array('id' => 3, 'title' => 'Öğretim Görevlisi', 'abbr' => 'Öğr. Gör.'),
            array('id' => 4, 'title' => 'Yardımcı Doçent', 'abbr' => 'Yrd. Doç.'),
            array('id' => 5, 'title' => 'Doçent', 'abbr' => 'Doç.'),
            array('id' => 6, 'title' => 'Profesör', 'abbr' => 'Prof.'),
        ));

        $this->command->info('Degree table seeded!');
	}

}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class CourseTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('courses')->delete();

        DB::table('courses')->insert(array(
            array('title' => 'Elektrik Devreler ve Elektronik', 'quota' => 40, 'instructor_id' => 8, 'department_id' => 1),
            array('title' => 'Fizik II', 'quota' => 60, 'instructor_id' => 2, 'department_id' => 1),
            array('title' => 'Matematik II', 'quota' => 50, 'instructor_id' => 3, 'department_id' => 1),
            array('title' => 'Olasılık ve İstatistiğe Giriş', 'quota' => 30, 'instructor_id' => 4, 'department_id' => 1),
            array('title' => 'Programlamaya Giriş II', 'quota' => 40, 'instructor_id' => 7, 'department_id' => 1),
            array('title' => 'Malzeme Kimyası', 'quota' => 20, 'instructor_id' => 11, 'department_id' => 2),
            array('title' => 'Fizik II', 'quota' => 50, 'instructor_id' => 12, 'department_id' => 2),
            array('title' => 'Matematik II', 'quota' => 55, 'instructor_id' => 13, 'department_id' => 2),
            array('title' => 'Bilgisayar Programlama', 'quota' => 30, 'instructor_id' => 7, 'department_id' => 2),
            array('title' => 'Elektrik Mühendisliği Temelleri II', 'quota' => 40, 'instructor_id' => 14, 'department_id' => 2),
            array('title' => 'Doğrusal Cebir', 'quota' => 30, 'instructor_id' => 15, 'department_id' => 2),
            array('title' => 'İngilizce II', 'quota' => 25, 'instructor_id' => 16, 'department_id' => 2),
        ));

        $this->command->info('Course table seeded!');
	}

}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class HeadTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('heads')->delete();

        // 1 bilg, 2 elekt
        // 2 dursun, 3 bünyamin, 4 recai, 5 gökhan
        DB::table('heads')->insert(array(
            array('department_id' => 1, 'instructor_id' => 3),
            array('department_id' => 2, 'instructor_id' => 4),
        ));

        $this->command->info('Head table seeded!');
	}

}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class ClassroomTableSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
        DB::table('classrooms')->delete();

        DB::table('classrooms')->insert(array(
            array('code' => 'A101', 'capacity' => 20),
            array('code' => 'A102', 'capacity' => 60),
            array('code' => 'A103', 'capacity' => 35),
            array('code' => 'A104', 'capacity' => 35),
            array('code' => 'A105', 'capacity' => 20),
            array('code' => 'B101', 'capacity' => 60),
            array('code' => 'B102', 'capacity' => 20),
            array('code' => 'B103', 'capacity' => 35),
            array('code' => 'B104', 'capacity' => 60),
            array('code' => 'B105', 'capacity' => 60),
        ));

        $this->command->info('Classroom table seeded!');
	}

}
